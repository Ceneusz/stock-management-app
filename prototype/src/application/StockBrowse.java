package application;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.util.HashMap;


public class StockBrowse extends javax.swing.JFrame {
    
    //constants for database connectivity
    static final String DB_URL = "jdbc:mysql://localhost:3306/appdb";
    static final String USER = "root";
    static final String PASS = "test";
    //most frequent sql requests saved as constants
    static final String ALL = "select * from stocks";
    static final String GROUPALL = "SELECT title,brand FROM stocks";
    
    //relevant variables
    Connection conn = null; //object, which provides connectivity
    PreparedStatement preparedStatement = null; //object, which enables requests
    ResultSet resultSet = null; //object, which receives data
    int colNumber, deleteItem;
    String stockID = ""; //variable, which stores currently clicked product
    String searchParameter = "Name"; //parameter for search
    boolean isSingularDisplay = true; /*display mode
        * singular - true
        * grouped - false
    */
    
    //function, which displays products on the table (singular mode)
    public void upDate(String SQLexpression){
        try{
            //initializing connection and request
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement(SQLexpression);
            
            //executing request and receiving data
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();
                        
            colNumber = Data.getColumnCount();
            
            //cleaning the table
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            RecordTable.setRowCount(0);
            
            //adding data to the table
            while(resultSet.next()){
                
                Vector columnData = new Vector();
                
                for (int i = 1; i <= colNumber; i++)
                {
                   columnData.add(resultSet.getString("StockID"));
                   columnData.add(resultSet.getString("title"));
                   columnData.add(resultSet.getString("brand"));
                   columnData.add(resultSet.getString("price"));
                }
                    RecordTable.addRow(columnData);
            }
        }
        catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    //function, which displays products on the table (grouped mode)
    public void getGroups(String SQLCommand){
        //declaring hashmap for storage purposes
        HashMap<String,Integer> products_map = new HashMap<String,Integer>();
        try{
            //connection init
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement(SQLCommand);

            //request execution
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();

            //finding unique pairs of names and products and counting
            //how often they appeared
            while(resultSet.next()){
                String product = resultSet.getString("title");
                String brand = resultSet.getString("brand");

                String entry = (product + ";" + brand);

                if(!products_map.containsKey(entry)){
                    products_map.put(entry, 1);
                }
                else{
                    int val = products_map.get(entry);
                    products_map.replace(entry, val+1);
                }
            }
            
            //reseting the table
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            RecordTable.setRowCount(0);
            
            //adding data from hashmap to the table
            for (HashMap.Entry<String, Integer> set: products_map.entrySet()) {

                Vector columnData = new Vector();
                String [] name_brand = set.getKey().split(";");
                columnData.add(name_brand[0]);
                columnData.add(name_brand[1]);
                columnData.add(set.getValue().toString());
                
                RecordTable.addRow(columnData);
            }

            
        }
        catch(SQLException e){
            //obligatory exception catch
        }

    }
    
    /*function executed in case of deleting or editing a product, if it  
      belongs to the order
    */
    public void updateOrdersPrice(float previous_price, float new_price){
        try{
            //finding order's ID number with sql statement
            preparedStatement = conn.prepareStatement("select * from orderstostocks where StockID=?");
            preparedStatement.setString(1, stockID);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            int orderID = resultSet.getInt("OrderID");

            //getting order's previous cost
            preparedStatement = conn.prepareStatement("select * from orders where OrderID=?");
            preparedStatement.setInt(1, orderID);
            resultSet = preparedStatement.executeQuery();
            resultSet.next();

            float sum = resultSet.getFloat("Price");
            
            //updating order's price
            preparedStatement = conn.prepareStatement("update orders set Price=? where OrderID=?");
            preparedStatement.setFloat(1, sum-previous_price+new_price);
            preparedStatement.setInt(2, orderID);
            preparedStatement.executeUpdate();
        }
        catch(SQLException e){
            
        }
    }
    
    //constructor of the class
    public StockBrowse() {
        initComponents();
        upDate(ALL);
        jSingularButton.doClick();
    }

    //function, which initialises contents of the form
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        displayGroup = new javax.swing.ButtonGroup();
        jTextPrice = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jBrandLabel = new javax.swing.JLabel();
        jTextName = new javax.swing.JTextField();
        jNameLabel = new javax.swing.JLabel();
        jButtonReset = new javax.swing.JButton();
        jTextBrand = new javax.swing.JTextField();
        jPriceLabel = new javax.swing.JLabel();
        jButtonAdd = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButtonDelete = new javax.swing.JButton();
        jButtonQuit = new javax.swing.JButton();
        jButtonEdit = new javax.swing.JButton();
        jSearchParameterComboBox = new javax.swing.JComboBox<>();
        jButtonShowAll = new javax.swing.JButton();
        jTextSearchField = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jSingularButton = new javax.swing.JRadioButton();
        jGroupedButton = new javax.swing.JRadioButton();
        jAmountLabel = new javax.swing.JLabel();
        jAmountSpinner = new javax.swing.JSpinner();
        jSeparator1 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(1280, 720));

        jTextPrice.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Brand", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setHeaderValue("ID");
            jTable1.getColumnModel().getColumn(1).setHeaderValue("Name");
            jTable1.getColumnModel().getColumn(2).setHeaderValue("Brand");
            jTable1.getColumnModel().getColumn(3).setHeaderValue("Price");
        }

        jBrandLabel.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jBrandLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jBrandLabel.setText("Brand:");

        jTextName.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jNameLabel.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jNameLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jNameLabel.setText("Product's name:");

        jButtonReset.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonReset.setText("Reset");
        jButtonReset.setIconTextGap(15);
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jTextBrand.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jPriceLabel.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jPriceLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jPriceLabel.setText("Price:");

        jButtonAdd.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setIconTextGap(15);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149254.png"))); // NOI18N

        jButtonDelete.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonDelete.setText("Delete");
        jButtonDelete.setIconTextGap(15);
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jButtonQuit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonQuit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149158.png"))); // NOI18N
        jButtonQuit.setText("Quit");
        jButtonQuit.setIconTextGap(15);
        jButtonQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitActionPerformed(evt);
            }
        });

        jButtonEdit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonEdit.setText("Edit");
        jButtonEdit.setIconTextGap(15);
        jButtonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditActionPerformed(evt);
            }
        });

        jSearchParameterComboBox.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jSearchParameterComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Name", "Brand" }));
        jSearchParameterComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSearchParameterComboBoxActionPerformed(evt);
            }
        });

        jButtonShowAll.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonShowAll.setText("Show all");
        jButtonShowAll.setIconTextGap(15);
        jButtonShowAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonShowAllActionPerformed(evt);
            }
        });

        jButtonSearch.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonSearch.setText("Search");
        jButtonSearch.setIconTextGap(15);
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });

        displayGroup.add(jSingularButton);
        jSingularButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jSingularButton.setText("Singular display");
        jSingularButton.setToolTipText("");
        jSingularButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSingularButtonActionPerformed(evt);
            }
        });

        displayGroup.add(jGroupedButton);
        jGroupedButton.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jGroupedButton.setText("Grouped display");
        jGroupedButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jGroupedButtonActionPerformed(evt);
            }
        });

        jAmountLabel.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jAmountLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jAmountLabel.setText("Amount:");

        jAmountSpinner.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jAmountSpinner.setModel(new javax.swing.SpinnerNumberModel(1, 1, null, 1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSingularButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jGroupedButton)
                .addGap(341, 341, 341))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jSearchParameterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(jTextSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonShowAll, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBrandLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jNameLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPriceLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jAmountLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(54, 54, 54)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextBrand, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                            .addComponent(jTextName, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                            .addComponent(jAmountSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextPrice))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jButtonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jButtonDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(44, 44, 44))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(75, 75, 75))))
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 677, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 952, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jSingularButton)
                    .addComponent(jGroupedButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextName, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jNameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextBrand, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jBrandLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(13, 13, 13)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jPriceLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jAmountLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jAmountSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButtonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButtonSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonShowAll, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jSearchParameterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //event, which changes values in text fields, when order in the table is clicked
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        if(isSingularDisplay){
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            int Row = jTable1.getSelectedRow();   
            jTextName.setText(RecordTable.getValueAt(Row, 1).toString());
            jTextBrand.setText(RecordTable.getValueAt(Row, 2).toString());
            jTextPrice.setText(RecordTable.getValueAt(Row, 3).toString());
            stockID = RecordTable.getValueAt(Row, 0).toString();
        }
        else{
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            int Row = jTable1.getSelectedRow();   
            jTextName.setText(RecordTable.getValueAt(Row, 0).toString());
            jTextBrand.setText(RecordTable.getValueAt(Row, 1).toString());
            jTextPrice.setText("");
        }
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        jTextName.setText("");
        jTextBrand.setText("");
        jTextPrice.setText("");
    }//GEN-LAST:event_jButtonResetActionPerformed

    //event, which adds products to the database
    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        try{
            //setting up sql statement and providing input from text fields
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("insert into stocks(Title,Brand,"
                                +"Price,Available)value(?,?,?,1)");
            preparedStatement.setString(1, jTextName.getText());
            preparedStatement.setString(2, jTextBrand.getText());
            preparedStatement.setString(3, jTextPrice.getText());
            
            //getting value of spinner
            int v = Integer.parseInt(jAmountSpinner.getValue().toString());
            
            //adding v products to the database
            for(int i = 0; i<v; i++){
                preparedStatement.executeUpdate();
            }
            
            //updating the table
            JOptionPane.showMessageDialog(this,"Product Added");
            upDate(ALL);
        } catch (SQLException exception) {
            /*database automatically checks if all needed inputs are correct
              if they aren't, SQLException is triggered, and warning window shows up
            */
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
            JOptionPane.showMessageDialog(null, "Wrong input data");
        }   
    }//GEN-LAST:event_jButtonAddActionPerformed

    //handling 'delete' event
    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        try {
            //first, we check current display mode
            if(isSingularDisplay){
                boolean proceed = true;
                
                deleteItem = JOptionPane.showConfirmDialog(null,
                        "Do you want to delete this product?",
                        "Warning", JOptionPane.YES_NO_OPTION);
                
                if (deleteItem == JOptionPane.YES_OPTION) {
                    //first of all, we check if product belongs to any order
                    conn = DriverManager.getConnection(DB_URL, USER, PASS);
                    preparedStatement = conn.prepareStatement(
                            "select Available from stocks where StockID = ?");
                    preparedStatement.setString(1, stockID);
                    resultSet = preparedStatement.executeQuery();
                    resultSet.next();
                    int available = resultSet.getInt("Available");
                  
                    if(available == 0){
                        
                        int deleteItem1 = JOptionPane.showConfirmDialog(null,
                                "This product is part of some order.\n"
                                + "Do you want to proceed?",
                        "Warning", JOptionPane.YES_NO_OPTION);
                        if(deleteItem1 == JOptionPane.YES_OPTION){
                            //firstly, we get order's ID
                            preparedStatement = conn.prepareStatement(
                                    "select OrderID from orderstostocks"
                                            + " where StockID=?");
                            preparedStatement.setString(1, stockID);
                            resultSet = preparedStatement.executeQuery();
                            resultSet.next();
                            
                            int orderID = resultSet.getInt("OrderID");
                            
                            /*
                              then we retrieve product's price from the table
                              and update order's price
                            */
                            
                            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
                            int Row = jTable1.getSelectedRow();
                            String prod_price = RecordTable.getValueAt(Row, 3).toString();
                            updateOrdersPrice(Float.parseFloat(prod_price), 0);
                            
                            //after that, we sever product's connection to the
                            //order
                            preparedStatement = conn.prepareStatement(
                                    "delete from orderstostocks where StockID=?");
                            preparedStatement.setString(1, stockID);
                            preparedStatement.executeUpdate();
                        }
                        else{
                            proceed = false;
                        }
                        
                    }
                    if(proceed){
                        //here we are deleting the stock from database
                        preparedStatement = conn.prepareStatement(
                                "delete from stocks where StockID =?");
                        preparedStatement.setString(1, stockID);
                        preparedStatement.executeUpdate();
                        JOptionPane.showMessageDialog(this, "Product deleted");
                        upDate(ALL);
                    }
                }
            }
            else{
                deleteItem = JOptionPane.showConfirmDialog(null, 
                        "Do you want to delete\ngroup of products?",
                        "Warning", JOptionPane.YES_NO_OPTION);
                
                //in this case we just execute one delete request from database
                if (deleteItem == JOptionPane.YES_OPTION) {
                    conn = DriverManager.getConnection(DB_URL, USER, PASS);
                    DefaultTableModel RecordTable =
                            (DefaultTableModel)jTable1.getModel();
                    int Row = jTable1.getSelectedRow();
                    String old_name = RecordTable.getValueAt(Row, 0).toString();
                    String old_brand = RecordTable.getValueAt(Row, 1).toString();
                    preparedStatement = conn.prepareStatement(
                            "delete from stocks where Title =? AND Brand=?");
                    preparedStatement.setString(1, old_name);
                    preparedStatement.setString(2, old_brand);
                    preparedStatement.executeUpdate();
                    JOptionPane.showMessageDialog(this, "Products deleted");
                    getGroups(GROUPALL);
                }
            }
            
        } catch (SQLException ex) {
            //obligatory exception catch
            JOptionPane.showMessageDialog(this, ex);
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    //shutdown the form
    private void jButtonQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitActionPerformed

    //implementation of 'edit' event
    private void jButtonEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditActionPerformed
        try{
            if(isSingularDisplay){
                //useful variables
                String name = jTextName.getText();
                String brand = jTextBrand.getText();
                String price = jTextPrice.getText();
                
                //setting up the request and checking, if product belongs to
                //any order
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                preparedStatement = conn.prepareStatement("select * from stocks where "+
                        "StockID=?");
                preparedStatement.setString(1, stockID);
                resultSet = preparedStatement.executeQuery();
                resultSet.next();
                
                float previous_price = resultSet.getFloat("Price");
                int available = resultSet.getInt("Available");
                
                //setting up update request with data provided in text fields
                preparedStatement = conn.prepareStatement("update stocks set Title=?, Brand=?, "
                                    +"Price=? where StockID=?");
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, brand);
                preparedStatement.setString(3, price);
                preparedStatement.setString(4, stockID);
                
                // ';' is critical to one of the functions, therefore it's
                //forbidden to use
                if(name.contains(";") || brand.contains(";")){
                    throw new Exception();
                }
                //checking, if any of the fields had been left blank
                if(name.isBlank()||brand.isBlank()||price.isBlank()){
                    throw new SQLException();
                }
                
                //updating the table
                preparedStatement.executeUpdate();
                JOptionPane.showMessageDialog(this,"Product edited");
                upDate(ALL);
                
                if(available == 0){
                    //if product is in any order, cost of the order is updated 
                    updateOrdersPrice(previous_price, Float.parseFloat(price));
                }
                
            }
            else{
                //pretty similar to previous one, except we can't change the price
                //in this mode
                String name = jTextName.getText();
                String brand = jTextBrand.getText();
                
                DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
                int Row = jTable1.getSelectedRow();
                String old_name = RecordTable.getValueAt(Row, 0).toString();
                String old_brand = RecordTable.getValueAt(Row, 1).toString();
                
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                preparedStatement = conn.prepareStatement("update stocks set Title=?, Brand=?"+
                        " where Title=? AND Brand=?");
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, brand);
                preparedStatement.setString(3, old_name);
                preparedStatement.setString(4, old_brand);
                
                preparedStatement.executeUpdate();
                jGroupedButton.doClick();
                JOptionPane.showMessageDialog(this,"Group edited");
            }
        }
        //catching exceptions caused by not proper input
        catch (SQLException exception) {
             //System.err.println(ex);
            java.util.logging.Logger.getLogger(
                    ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
            JOptionPane.showMessageDialog(null, "Wrong input data");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "';' sign is forbidden");
        }
        
    }//GEN-LAST:event_jButtonEditActionPerformed

    //event which shows every database record in the table
    private void jButtonShowAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonShowAllActionPerformed
        if(isSingularDisplay){
            upDate(ALL);
        }
        else{
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            RecordTable.setRowCount(0);
            getGroups(GROUPALL);
        }
    }//GEN-LAST:event_jButtonShowAllActionPerformed

    //search fuctionality
    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        //first, the search parameter is checked
        String parameter = "";
            String sercz = jTextSearchField.getText();
            if ("Brand" == searchParameter){
                parameter = "brand";
            }
            if ("Name" == searchParameter){
                parameter = "title";
            }
        //then we send the request depending in which mode we are working
        if(isSingularDisplay){
            String mySQLcommand = "SELECT * FROM stocks WHERE "+ parameter +" LIKE '%" +sercz + "%'";

            upDate(mySQLcommand);
        }
        else{
            DefaultTableModel RecordTable = (DefaultTableModel)jTable1.getModel();
            RecordTable.setRowCount(0);
                    
            String mySQLcommand = "SELECT title,brand FROM stocks WHERE "+
                    parameter+" LIKE '%"+ sercz +"%'";
        
            getGroups(mySQLcommand);
        }
    }//GEN-LAST:event_jButtonSearchActionPerformed

    //button that switches to group display mode and changes table layout
    //it also disables 'add' button and 'price' text field
    private void jGroupedButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jGroupedButtonActionPerformed
        isSingularDisplay = false;
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Brand", "Quantity"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setHeaderValue("Name");
            jTable1.getColumnModel().getColumn(1).setHeaderValue("Brand");
            jTable1.getColumnModel().getColumn(2).setHeaderValue("Quantity");
        }
        
        getGroups(GROUPALL);
        
        jButtonAdd.setEnabled(false);
        jTextPrice.setEnabled(false);
        
    }//GEN-LAST:event_jGroupedButtonActionPerformed

    //selecting search parameter
    private void jSearchParameterComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSearchParameterComboBoxActionPerformed
        //'searchParameter' is updated, when option in combobox is selected
        searchParameter = jSearchParameterComboBox.getSelectedItem().toString();
    }//GEN-LAST:event_jSearchParameterComboBoxActionPerformed

    //button that switches to individual display mode and changes table layout
    //it also enables 'add' button and 'price' text field
    private void jSingularButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSingularButtonActionPerformed
        isSingularDisplay = true;
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Brand", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable1);
        if (jTable1.getColumnModel().getColumnCount() > 0) {
            jTable1.getColumnModel().getColumn(0).setHeaderValue("ID");
            jTable1.getColumnModel().getColumn(1).setHeaderValue("Name");
            jTable1.getColumnModel().getColumn(2).setHeaderValue("Brand");
            jTable1.getColumnModel().getColumn(3).setHeaderValue("Price");
        }
        
        jButtonAdd.setEnabled(true);
        jButtonEdit.setEnabled(true);
        jButtonDelete.setEnabled(true);
        jButtonSearch.setEnabled(true);
        jButtonShowAll.setEnabled(true);
        jButtonReset.setEnabled(true);
        jTextPrice.setEnabled(true);
        
        upDate(ALL);
    }//GEN-LAST:event_jSingularButtonActionPerformed


    //main function
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(StockBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(StockBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(StockBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(StockBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new StockBrowse().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup displayGroup;
    private javax.swing.JLabel jAmountLabel;
    private javax.swing.JSpinner jAmountSpinner;
    private javax.swing.JLabel jBrandLabel;
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonEdit;
    private javax.swing.JButton jButtonQuit;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JButton jButtonShowAll;
    private javax.swing.JRadioButton jGroupedButton;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jNameLabel;
    private javax.swing.JLabel jPriceLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jSearchParameterComboBox;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JRadioButton jSingularButton;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField jTextBrand;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextPrice;
    private javax.swing.JTextField jTextSearchField;
    // End of variables declaration//GEN-END:variables
}
