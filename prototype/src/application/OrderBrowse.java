/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static application.ClientsBrowse.DB_URL;
import static application.ClientsBrowse.PASS;
import static application.ClientsBrowse.USER;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;


/**
 *
 * @author Klient
 */
public class OrderBrowse extends javax.swing.JFrame {

    Connection conn = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    int colNumber, deleteItem;
    String orderID = null;
    Integer selectedOrderedID = null;
    Integer selectedAvailableID = null;
    float selectedOrderPrice, selectedOrderedPrice, selectedAvailablePrice;

    //Updating the main table, by getting all the orders data from database
    public void upDate() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("select * from orders");

            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();

            colNumber = Data.getColumnCount();

            DefaultTableModel RecordTable = (DefaultTableModel) jTableOrders.getModel();
            RecordTable.setRowCount(0);
            
            while (resultSet.next()) {

                Vector columnData = new Vector();

                for (int i = 1; i <= colNumber; i++) {
                    columnData.add(resultSet.getString("OrderID"));
                    columnData.add(resultSet.getString("Date"));
                    columnData.add(resultSet.getString("Price"));
                    String clientID = resultSet.getString("ClientID");
                    columnData.add(clientID);
                    columnData.add(resultSet.getString("Status"));
                    columnData.add(resultSet.getString("Adress"));
                    conn = DriverManager.getConnection(DB_URL, USER, PASS);
                    //getting the client's full nabe based on clientID number from the order
                    PreparedStatement preparedStatementC = conn.prepareStatement("select "+
                            "* from clients where ClientID=?");
                    preparedStatementC.setString(1, clientID);
                    ResultSet resultSetC = preparedStatementC.executeQuery();
                    String fullName = "";
                    while (resultSetC.next()) {
                    String name = resultSetC.getString("Name");
                    String surname= resultSetC.getString("Surname");
                    fullName = name + " " + surname;
                }
                    columnData.add( fullName);
                }
                RecordTable.addRow(columnData);
            }
            upDateAvailable();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    //updating the table containing all the products realted to the order
    public void upDateOrdered() {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // searching for all realted stocks
            preparedStatement = conn.prepareStatement("select * from orderstostocks where OrderID=?");

            preparedStatement.setString(1, orderID);
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();
            colNumber = Data.getColumnCount();
            DefaultTableModel RecordTable = (DefaultTableModel) jTableOrdered.getModel();

            RecordTable.setRowCount(0);

            while (resultSet.next()) {

                Vector columnData = new Vector();
                String stockID = resultSet.getString("StockID");
                // searching for the details on the stock
                PreparedStatement preparedStatementStocks = conn.prepareStatement("select"
                        + " * from stocks where StockID=?");
                preparedStatementStocks.setString(1, stockID);
                ResultSet resultSetStock = preparedStatementStocks.executeQuery();
                resultSetStock.next();
                for (int i = 1; i <= colNumber; i++) {
                    String title = resultSetStock.getString("Title");
                    String price = resultSetStock.getString("Price");
                    String brand = resultSetStock.getString("Brand");
                    columnData.add(stockID);
                    columnData.add(title);
                    columnData.add(brand);
                    columnData.add(price);
                }
                RecordTable.addRow(columnData);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    //updating the table that displays all the available products
    public void upDateAvailable() {
        try {
            preparedStatement = conn.prepareStatement("SELECT DISTINCT Price,Title,Brand from "
                    +"stocks where Available=1");
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();
            colNumber = Data.getColumnCount();
            DefaultTableModel RecordTable = (DefaultTableModel) jTableAvailable.getModel();
            RecordTable.setRowCount(0);
            while (resultSet.next()) {
                Vector columnData = new Vector();
                for (int i = 1; i <= colNumber; i++) {
                    String title = resultSet.getString("Title");
                    String price = resultSet.getString("Price");
                    String brand = resultSet.getString("Brand");
                    // getting the details regarding specific produst
                    preparedStatement = conn.prepareStatement(
                            "SELECT * FROM stocks WHERE Available=1 AND"
                                    + " cast(Price as DECIMAL) = cast(? AS DECIMAL)"
                                    + " AND Title=? AND Brand=?");
                    preparedStatement.setString(1, price);
                    preparedStatement.setString(2, title);
                    preparedStatement.setString(3, brand);
                    ResultSet resultSetD = preparedStatement.executeQuery();
                    int setSize=0;
                    while(resultSetD.next()){
                        setSize++;
                    }
                    columnData.add(setSize);
                    columnData.add(title);
                    columnData.add(brand);
                    columnData.add(price);
                }
                RecordTable.addRow(columnData);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    // moving a product to the set of available products
    public void moveToAvailable(int ID) {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //updating product as available
            preparedStatement = conn.prepareStatement("update stocks set Available=1 where StockID=?");
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
            //deleting connection to order
            preparedStatement = conn.prepareStatement("delete from orderstostocks where StockID =?");
            preparedStatement.setInt(1, ID);
            preparedStatement.executeUpdate();
            upDateOrdered();
            upDateAvailable();
        } catch (SQLException exception) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        }
    }
    
    public boolean isNotFutureDate(String date){
        boolean isFuture = true;
        try{
        isFuture = LocalDate.parse(date).isAfter(LocalDate.now());
        
        }catch(DateTimeParseException e ){
            JOptionPane.showMessageDialog(this, "Incorrect date structure.");
        }
        return !isFuture;
    }

    public OrderBrowse() {
        initComponents();
        upDate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonEdit = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jButtonQuit = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableOrders = new javax.swing.JTable();
        jButtonAdd = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextPrice = new javax.swing.JTextField();
        jTextDate = new javax.swing.JTextField();
        jTextClientID = new javax.swing.JTextField();
        jTextAdress = new javax.swing.JTextField();
        jButtonToAvailable = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jButtonReset = new javax.swing.JButton();
        jButtonToOrdered = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableOrdered = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jTableAvailable = new javax.swing.JTable();
        jComboBoxStatus = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(1280, 720));

        jButtonEdit.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonEdit.setText("Edit");
        jButtonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149345.png"))); // NOI18N

        jButtonQuit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonQuit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149158.png"))); // NOI18N
        jButtonQuit.setText("Quit");
        jButtonQuit.setIconTextGap(15);
        jButtonQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitActionPerformed(evt);
            }
        });

        jButtonDelete.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonDelete.setText("Delete");
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("  Ordered products:");

        jTableOrders.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Order ID", "Date", "Price", "Client ID", "Status", "Adress", "Client Name"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableOrders.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableOrdersMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTableOrders);
        if (jTableOrders.getColumnModel().getColumnCount() > 0) {
            jTableOrders.getColumnModel().getColumn(0).setResizable(false);
            jTableOrders.getColumnModel().getColumn(0).setPreferredWidth(5);
            jTableOrders.getColumnModel().getColumn(1).setResizable(false);
            jTableOrders.getColumnModel().getColumn(1).setPreferredWidth(20);
            jTableOrders.getColumnModel().getColumn(2).setResizable(false);
            jTableOrders.getColumnModel().getColumn(2).setPreferredWidth(20);
            jTableOrders.getColumnModel().getColumn(3).setResizable(false);
            jTableOrders.getColumnModel().getColumn(3).setPreferredWidth(20);
            jTableOrders.getColumnModel().getColumn(4).setResizable(false);
            jTableOrders.getColumnModel().getColumn(4).setPreferredWidth(30);
        }

        jButtonAdd.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Status:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Date:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Price of order:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Adress:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Client ID:");

        jTextPrice.setEditable(false);

        jButtonToAvailable.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonToAvailable.setText("Move to available");
        jButtonToAvailable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonToAvailableActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Available products:");

        jButtonReset.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonReset.setText("Reset");
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jButtonToOrdered.setFont(new java.awt.Font("Leelawadee UI", 1, 11)); // NOI18N
        jButtonToOrdered.setText("Move to ordered");
        jButtonToOrdered.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonToOrderedActionPerformed(evt);
            }
        });

        jTableOrdered.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Stock ID", "Title", "Brand", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableOrdered.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableOrderedMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableOrdered);

        jTableAvailable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Quantity", "Title", "Brand", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableAvailable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableAvailableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(jTableAvailable);

        jComboBoxStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Order placed", "In progress", "Shipped", "Completed" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(22, 22, 22)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButtonDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButtonReset, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(50, 50, 50)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButtonToAvailable, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(jButtonToOrdered, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                    .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jTextDate, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jTextClientID, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                                            .addComponent(jTextAdress, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE))))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jButtonAdd, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jComboBoxStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 882, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(12, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(12, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel5)
                                    .addComponent(jTextDate, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel8)
                                    .addComponent(jTextClientID, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(jTextAdress, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jComboBoxStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButtonToAvailable)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jButtonToOrdered))
                        .addGap(8, 8, 8)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                        .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //editing chosen order
    private void jButtonEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditActionPerformed
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            // pasiing to the sql command all the data from text boxes
            preparedStatement = conn.prepareStatement("update orders set Date=?,ClientID=?,Status=?,Adress=? where OrderID=?");
            if (isNotFutureDate(jTextDate.getText())){
            preparedStatement.setString(1, jTextDate.getText());
            }else{
                throw new IllegalArgumentException();
            }
            preparedStatement.setString(2, jTextClientID.getText());
            preparedStatement.setString(3, jComboBoxStatus.getSelectedItem().toString());
            preparedStatement.setString(4, jTextAdress.getText());
            preparedStatement.setString(5, orderID);

            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(this, "Order edited");
            upDate();
        } catch (SQLException exception) {
            JOptionPane.showMessageDialog(this, "Wrong input data");
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        } catch (IllegalArgumentException e){
            JOptionPane.showMessageDialog(this, "Future dates are not allowed.");
        }
    }//GEN-LAST:event_jButtonEditActionPerformed

    //closing current window
    private void jButtonQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitActionPerformed

    //deleting selected order
    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        try {
            deleteItem = JOptionPane.showConfirmDialog(null, "Do you want to delete this order?",
                    "Warning", JOptionPane.YES_NO_OPTION);
            if (deleteItem == JOptionPane.YES_OPTION) {
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                //deleting all the connections to the products
                preparedStatement = conn.prepareStatement("select * from orderstostocks where OrderID =?");
                preparedStatement.setString(1, orderID);
                ResultSet resultSetD = preparedStatement.executeQuery();
                while (resultSetD.next()) {
                    Integer stockID = resultSetD.getInt("StockID");
                    moveToAvailable(stockID);
                }
                //deleting selected order
                preparedStatement = conn.prepareStatement("delete from orders where OrderID =?");
                preparedStatement.setString(1, orderID);
                preparedStatement.executeUpdate();

                JOptionPane.showMessageDialog(this, "Order deleted");
            }

            upDate();
        } catch (SQLException ex) {
            System.err.println(ex);
            JOptionPane.showMessageDialog(this, "Wrong input data");
        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    //adding order
    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("insert into orders(Date,Price,ClientID,Status,"
                    + "Adress)value(?,?,?,?,?)");
                        if (isNotFutureDate(jTextDate.getText())){
            preparedStatement.setString(1, jTextDate.getText());
            }else{
                throw new IllegalArgumentException();
            
            }
            preparedStatement.setString(2, "0");
            preparedStatement.setString(3, jTextClientID.getText());
            preparedStatement.setString(4, jComboBoxStatus.getSelectedItem().toString());
            preparedStatement.setString(5, jTextAdress.getText());

            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(this, "Order Added");
            upDate();
        } catch (SQLException exception) {
            JOptionPane.showMessageDialog(this, "Wrong input data");
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        }catch (IllegalArgumentException e){
            JOptionPane.showMessageDialog(this, "Future dates are not allowed.");
        }
    }//GEN-LAST:event_jButtonAddActionPerformed

    //updating the price of the order and moving product to available set
    private void jButtonToAvailableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonToAvailableActionPerformed
        moveToAvailable(selectedOrderedID);
        try {
            selectedOrderPrice -= selectedOrderedPrice;
            preparedStatement = conn.prepareStatement("update orders set Price=? where OrderID=?");
            preparedStatement.setFloat(1, selectedOrderPrice);
            preparedStatement.setString(2, orderID);
            preparedStatement.executeUpdate();
            jTextPrice.setText(String.valueOf(selectedOrderPrice));
            JOptionPane.showMessageDialog(this, "Stock moved to available.");
            selectedOrderedID=null;
            upDate();
        } catch (SQLException exception) {
            //System.err.println(ex);
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        }
    }//GEN-LAST:event_jButtonToAvailableActionPerformed

    // reseting all the text fields
    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        jTextDate.setText("");
        jTextPrice.setText("");
        jTextClientID.setText("");
        jTextAdress.setText("");
        orderID = null;
        DefaultTableModel RecordTable = (DefaultTableModel) jTableOrdered.getModel();
        RecordTable.setRowCount(0);
        selectedOrderedID = null;
        upDate();
    }//GEN-LAST:event_jButtonResetActionPerformed

    // connecting selected product to selected order
    private void jButtonToOrderedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonToOrderedActionPerformed
        try {
            if (selectedAvailableID != null) {
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                //specifying product as unavailable
                preparedStatement = conn.prepareStatement("update stocks set Available=0 where StockID=?");
                preparedStatement.setInt(1, selectedAvailableID);
                preparedStatement.executeUpdate();
                //creating connection between the product and the order
                preparedStatement = conn.prepareStatement("insert into orderstostocks(StockID,OrderID)value(?,?)");
                preparedStatement.setInt(1, selectedAvailableID);
                preparedStatement.setString(2, orderID);
                preparedStatement.executeUpdate();
                //updating total cost of order
                selectedOrderPrice += selectedAvailablePrice;
                preparedStatement = conn.prepareStatement("update orders set Price=? where OrderID=?");
                preparedStatement.setFloat(1, selectedOrderPrice);
                preparedStatement.setString(2, orderID);
                preparedStatement.executeUpdate();
                JOptionPane.showMessageDialog(this, "Stock moved to ordered.");
                jTextPrice.setText(String.valueOf(selectedOrderPrice));
                selectedAvailableID=null;
                upDate();
                upDateOrdered();
            }
        } catch (SQLException exception) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        }
    }//GEN-LAST:event_jButtonToOrderedActionPerformed

    //passing the data form the main table to text boxes
    private void jTableOrdersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableOrdersMouseClicked
        DefaultTableModel RecordTable = (DefaultTableModel) jTableOrders.getModel();
        int Row = jTableOrders.getSelectedRow();
        jTextDate.setText(RecordTable.getValueAt(Row, 1).toString());
        jTextPrice.setText(RecordTable.getValueAt(Row, 2).toString());
        selectedOrderPrice = Float.parseFloat(RecordTable.getValueAt(Row, 2).toString());
        jTextClientID.setText(RecordTable.getValueAt(Row, 3).toString());
        jComboBoxStatus.setSelectedItem(RecordTable.getValueAt(Row, 4).toString());
        jTextAdress.setText(RecordTable.getValueAt(Row, 5).toString());
        orderID = RecordTable.getValueAt(Row, 0).toString();
        upDateOrdered();
    }//GEN-LAST:event_jTableOrdersMouseClicked

    //selecting ordered product
    private void jTableOrderedMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableOrderedMouseClicked
        DefaultTableModel RecordTable = (DefaultTableModel) jTableOrdered.getModel();
        int Row = jTableOrdered.getSelectedRow();
        selectedOrderedID = Integer.parseInt(RecordTable.getValueAt(Row, 0).toString());
        selectedOrderedPrice = Float.parseFloat(RecordTable.getValueAt(Row, 3).toString());
    }//GEN-LAST:event_jTableOrderedMouseClicked

    //selecting available product
    private void jTableAvailableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAvailableMouseClicked
        DefaultTableModel RecordTable = (DefaultTableModel) jTableAvailable.getModel();
        int Row = jTableAvailable.getSelectedRow();
        try{
        //finding ID of only one product from the selected group
        preparedStatement = conn.prepareStatement(
                "SELECT stockID FROM stocks WHERE Title=? AND Brand =? AND"
                        + " cast(Price as DECIMAL) = cast(? AS DECIMAL)"
                        + " AND Available=1 LIMIT 1");
        preparedStatement.setString(1, RecordTable.getValueAt(Row, 1).toString());
        preparedStatement.setString(2, RecordTable.getValueAt(Row, 2).toString());
        preparedStatement.setString(3, RecordTable.getValueAt(Row, 3).toString());

        ResultSet resultSetA = preparedStatement.executeQuery();
        resultSetA.next();
        selectedAvailableID = resultSetA.getInt("StockID");
        

        selectedAvailablePrice = Float.parseFloat(RecordTable.getValueAt(Row, 3).toString());
        
        } catch (SQLException exception) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
        }
    }//GEN-LAST:event_jTableAvailableMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OrderBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OrderBrowse().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonEdit;
    private javax.swing.JButton jButtonQuit;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonToAvailable;
    private javax.swing.JButton jButtonToOrdered;
    private javax.swing.JComboBox<String> jComboBoxStatus;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JTable jTableAvailable;
    private javax.swing.JTable jTableOrdered;
    private javax.swing.JTable jTableOrders;
    private javax.swing.JTextField jTextAdress;
    private javax.swing.JTextField jTextClientID;
    private javax.swing.JTextField jTextDate;
    private javax.swing.JTextField jTextPrice;
    // End of variables declaration//GEN-END:variables
}
