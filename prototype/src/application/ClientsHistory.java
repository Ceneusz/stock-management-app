package application;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.HashMap;

//parameters for connection with database
import static application.ClientsBrowse.DB_URL;
import static application.ClientsBrowse.PASS;
import static application.ClientsBrowse.USER;

public class ClientsHistory extends javax.swing.JFrame {
    
    //relevant class parameters
    Connection conn = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    int colNumber;
    String orderID = null;
    String clientID_ = ClientsBrowse.clientID; //ID numer of the client, whose
    // purchase history we are browsing
    float selectedOrderPrice, selectedOrderedPrice, selectedAvailablePrice;
    
    /*Constructor for window. It loads needed data and performs function, which
    initialises the form
     */
    public ClientsHistory() {
        initComponents();
        upDate();
        loadClientData();
    }

    /**
    Here is the function which creates form's contents
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        jComboBoxType = new javax.swing.JComboBox<>();
        jTextCardNumber2 = new javax.swing.JTextField();
        jTextAdress = new javax.swing.JTextField();
        jTextCardNumber4 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextCardNumber3 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextName = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextSurname = new javax.swing.JTextField();
        jTextCardNumber1 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        jButtonQuit = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jTextCardNumber5 = new javax.swing.JTextField();
        jTextAdress1 = new javax.swing.JTextField();
        jTextCardNumber6 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextCardNumber7 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jTextName1 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextSurname1 = new javax.swing.JTextField();
        jTextCardNumber8 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextType = new javax.swing.JTextField();

        jLabel9.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Type");

        jComboBoxType.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jComboBoxType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Individual", "Company" }));

        jTextCardNumber2.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextAdress.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextCardNumber4.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Credit card:");

        jTextCardNumber3.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel3.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Name:");

        jTextName.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Surname:");

        jTextSurname.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextCardNumber1.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel8.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Adress:");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Order ID", "Date", "Price", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Title", "Brand", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable2);
        if (jTable2.getColumnModel().getColumnCount() > 0) {
            jTable2.getColumnModel().getColumn(0).setResizable(false);
            jTable2.getColumnModel().getColumn(1).setResizable(false);
            jTable2.getColumnModel().getColumn(2).setResizable(false);
            jTable2.getColumnModel().getColumn(3).setResizable(false);
        }

        jButtonQuit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonQuit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149158.png"))); // NOI18N
        jButtonQuit.setText("Quit");
        jButtonQuit.setIconTextGap(15);
        jButtonQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Type");

        jTextCardNumber5.setEditable(false);
        jTextCardNumber5.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextAdress1.setEditable(false);
        jTextAdress1.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextCardNumber6.setEditable(false);
        jTextCardNumber6.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Credit card:");

        jTextCardNumber7.setEditable(false);
        jTextCardNumber7.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Name:");

        jTextName1.setEditable(false);
        jTextName1.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Surname:");

        jTextSurname1.setEditable(false);
        jTextSurname1.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextCardNumber8.setEditable(false);
        jTextCardNumber8.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Adress:");

        jTextType.setEditable(false);
        jTextType.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jScrollPane3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(30, 30, 30)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jTextSurname1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextName1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jTextCardNumber8, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextAdress1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jTextCardNumber5, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextCardNumber7, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jTextCardNumber6, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(32, 32, 32)
                                    .addComponent(jTextType, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(70, 70, 70)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonQuit, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(52, 52, 52))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                        .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextName1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextSurname1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextAdress1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextCardNumber8, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextCardNumber5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextCardNumber7, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextCardNumber6, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextType, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(32, 32, 32))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //This function loads data from MySQL database about orders
    public void upDate() {
        
        try {
            //getting connection with database, sending and executing request
            //for client's orders
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("select * from orders"+
                    " WHERE ClientID = " + clientID_);
            resultSet = preparedStatement.executeQuery();
            
            //results of request are later displayed on table
            ResultSetMetaData Data = resultSet.getMetaData();
            colNumber = Data.getColumnCount();
            DefaultTableModel RecordTable = (DefaultTableModel) jTable1.getModel();
            RecordTable.setRowCount(0); //first, table is cleared

            //then data received from database is added to table
            while (resultSet.next()) {

                Vector columnData = new Vector();

                for (int i = 1; i <= colNumber; i++) {
                    columnData.add(resultSet.getString("OrderID"));
                    columnData.add(resultSet.getString("Date"));
                    columnData.add(resultSet.getString("Price"));
                    columnData.add(resultSet.getString("Status"));
                }
                RecordTable.addRow(columnData);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    //this function loads informations related to client
    public void loadClientData(){
        try {
            //first we send request to get clients's data
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("SELECT * FROM clients" +
                    " WHERE ClientID = " + clientID_);
            
            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();
            
            colNumber = Data.getColumnCount();
            
            resultSet.next();
            
            //results are later displayed in text fields
            
            jTextName1.setText(resultSet.getString("Name"));
            jTextSurname1.setText(resultSet.getString("Surname"));
            jTextAdress1.setText(resultSet.getString("Adress"));
            String card_number = resultSet.getString("CardNumber");
            jTextCardNumber5.setText(card_number.substring(0, 4));
            jTextCardNumber6.setText(card_number.substring(4, 8));
            jTextCardNumber7.setText(card_number.substring(8, 12));
            jTextCardNumber8.setText(card_number.substring(12));
            jTextType.setText(resultSet.getString("Type"));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    //function which displays currently edited data
    public void upDateList(){
        try {
            //first, we are getting ids of products belonging to currently
            //displayed orders
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("select * from orderstostocks where OrderID=?");
            preparedStatement.setString(1, orderID);
            resultSet = preparedStatement.executeQuery();
            
            //then received data is displayed in dedicated table
            ResultSetMetaData Data = resultSet.getMetaData();
            colNumber = Data.getColumnCount();
            DefaultTableModel RecordTable = (DefaultTableModel) jTable2.getModel();

            //initially, we reset table
            RecordTable.setRowCount(0);

            //then we are parsing through ID numbers of products and receiving
            //their data for display
            while (resultSet.next()) {

                Vector columnData = new Vector();
                String stockID = resultSet.getString("StockID");
                PreparedStatement preparedStatementStocks = conn.prepareStatement("select"
                        + " * from stocks where StockID=?");
                preparedStatementStocks.setString(1, stockID);
                ResultSet resultSetStock = preparedStatementStocks.executeQuery();
                resultSetStock.next();
                for (int i = 1; i <= colNumber; i++) {
                    String title = resultSetStock.getString("Title");
                    String price = resultSetStock.getString("Price");
                    String brand = resultSetStock.getString("Brand");
                    columnData.add(stockID);
                    columnData.add(title);
                    columnData.add(brand);
                    columnData.add(price);
                }
                RecordTable.addRow(columnData);
            }

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        //event connected to main table, which selects the order from list
        //later the orderID variable is updated and function which updates the
        //smaller table is launched
        DefaultTableModel RecordTable = (DefaultTableModel) jTable1.getModel();
        int Row = jTable1.getSelectedRow();
        orderID = RecordTable.getValueAt(Row, 0).toString();
        upDateList();
    }//GEN-LAST:event_jTable1MouseClicked

    private void jButtonQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientsHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientsHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientsHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientsHistory.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClientsHistory().setVisible(true);
            }
        });
    }
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonQuit;
    private javax.swing.JComboBox<String> jComboBoxType;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JTextField jTextAdress;
    private javax.swing.JTextField jTextAdress1;
    private javax.swing.JTextField jTextCardNumber1;
    private javax.swing.JTextField jTextCardNumber2;
    private javax.swing.JTextField jTextCardNumber3;
    private javax.swing.JTextField jTextCardNumber4;
    private javax.swing.JTextField jTextCardNumber5;
    private javax.swing.JTextField jTextCardNumber6;
    private javax.swing.JTextField jTextCardNumber7;
    private javax.swing.JTextField jTextCardNumber8;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextName1;
    private javax.swing.JTextField jTextSurname;
    private javax.swing.JTextField jTextSurname1;
    private javax.swing.JTextField jTextType;
    // End of variables declaration//GEN-END:variables
}
