package application;

import javax.swing.JOptionPane;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Vector;

public class ClientsBrowse extends javax.swing.JFrame {

    static final String DB_URL = "jdbc:mysql://localhost:3306/appdb";
    static final String USER = "root";
    static final String PASS = "test";
    static final String ALL = "select * from clients";

    Connection conn = null;
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;
    int colNumber, deleteItem;
    public static String clientID;
    String searchParameter = "ID";

    public void upDate(String a) {
        try {
            //establishing a connection
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement(a);

            resultSet = preparedStatement.executeQuery();
            ResultSetMetaData Data = resultSet.getMetaData();

            //displaying the data from result set in the table
            colNumber = Data.getColumnCount();

            DefaultTableModel RecordTable = (DefaultTableModel) jTableClients.getModel();
            RecordTable.setRowCount(0);

            while (resultSet.next()) {

                Vector columnData = new Vector();

                for (int i = 1; i <= colNumber; i++) {
                    columnData.add(resultSet.getString("ClientID"));
                    columnData.add(resultSet.getString("Name"));
                    columnData.add(resultSet.getString("Surname"));
                    columnData.add(resultSet.getString("Adress"));
                    columnData.add(resultSet.getString("CardNumber"));
                    columnData.add(resultSet.getString("Type"));
                }
                RecordTable.addRow(columnData);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public ClientsBrowse() {
        initComponents();
        upDate(ALL);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonAdd = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jButtonQuit = new javax.swing.JButton();
        jTextAdress = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTextName = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTextSurname = new javax.swing.JTextField();
        jTextCardNumber1 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jButtonOrdersHistory = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
        jButtonEdit = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableClients = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jButtonReset = new javax.swing.JButton();
        jComboBoxType = new javax.swing.JComboBox<>();
        jSeparator1 = new javax.swing.JSeparator();
        jSearchParameterComboBox = new javax.swing.JComboBox<>();
        jButtonShowAll = new javax.swing.JButton();
        jTextSearchField = new javax.swing.JTextField();
        jButtonSearch = new javax.swing.JButton();
        jTextCardNumber2 = new javax.swing.JTextField();
        jTextCardNumber4 = new javax.swing.JTextField();
        jTextCardNumber3 = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonAdd.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonAdd.setText("Add");
        jButtonAdd.setIconTextGap(15);
        jButtonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAddActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149447.png"))); // NOI18N

        jButtonQuit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonQuit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/149158.png"))); // NOI18N
        jButtonQuit.setText("Quit");
        jButtonQuit.setIconTextGap(15);
        jButtonQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonQuitActionPerformed(evt);
            }
        });

        jTextAdress.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Credit card:");

        jLabel3.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Name:");

        jTextName.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Surname:");

        jTextSurname.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N

        jTextCardNumber1.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jTextCardNumber1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextCardNumber1KeyTyped(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Adress:");

        jButtonOrdersHistory.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonOrdersHistory.setText("Orders History");
        jButtonOrdersHistory.setIconTextGap(15);
        jButtonOrdersHistory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOrdersHistoryActionPerformed(evt);
            }
        });

        jButtonDelete.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonDelete.setText("Delete");
        jButtonDelete.setIconTextGap(15);
        jButtonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDeleteActionPerformed(evt);
            }
        });

        jButtonEdit.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonEdit.setText("Edit");
        jButtonEdit.setIconTextGap(15);
        jButtonEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEditActionPerformed(evt);
            }
        });

        jTableClients.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Name", "Surname", "Adress", "CardNumber", "Type"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableClients.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableClientsMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableClients);
        if (jTableClients.getColumnModel().getColumnCount() > 0) {
            jTableClients.getColumnModel().getColumn(0).setResizable(false);
            jTableClients.getColumnModel().getColumn(0).setPreferredWidth(15);
        }

        jLabel9.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Type");

        jButtonReset.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonReset.setText("Reset");
        jButtonReset.setIconTextGap(15);
        jButtonReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonResetActionPerformed(evt);
            }
        });

        jComboBoxType.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jComboBoxType.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Individual", "Company" }));

        jSearchParameterComboBox.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jSearchParameterComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "ID", "Name", "Surname", "Adress", "Card Number", "Type" }));
        jSearchParameterComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jSearchParameterComboBoxActionPerformed(evt);
            }
        });

        jButtonShowAll.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonShowAll.setText("Show all");
        jButtonShowAll.setIconTextGap(15);
        jButtonShowAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonShowAllActionPerformed(evt);
            }
        });

        jTextSearchField.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jButtonSearch.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jButtonSearch.setText("Search");
        jButtonSearch.setIconTextGap(15);
        jButtonSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSearchActionPerformed(evt);
            }
        });

        jTextCardNumber2.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jTextCardNumber2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextCardNumber2KeyTyped(evt);
            }
        });

        jTextCardNumber4.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jTextCardNumber4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextCardNumber4KeyTyped(evt);
            }
        });

        jTextCardNumber3.setFont(new java.awt.Font("Microsoft New Tai Lue", 0, 18)); // NOI18N
        jTextCardNumber3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextCardNumber3KeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextSurname, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextName, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextCardNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextAdress, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonOrdersHistory, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonDelete, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonReset, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 952, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jTextCardNumber2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextCardNumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextCardNumber4, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(jComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 37, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jSearchParameterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(46, 46, 46)
                        .addComponent(jTextSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonShowAll, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 677, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(16, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextName, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jTextSurname, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButtonEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextAdress, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextCardNumber1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonReset, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextCardNumber2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextCardNumber4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextCardNumber3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonOrdersHistory, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBoxType, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addComponent(jButtonQuit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jButtonSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextSearchField, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButtonShowAll, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jSearchParameterComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    boolean checkCardNumber(String number) {
        /* Validating the card numberpassed int to the text box
           A card number has to be 16 digit long
         */
        if (number.length() != 16) {
            return false;
        }

        for (int i = 0; i < number.length(); i++) {
            char c = number.charAt(i);

            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    boolean checkName(String name) {
        /* Validating the name
           All the passed characters has to be letters or space
           and the first one must be uppercase
         */

        char first_letter = name.charAt(0);
        if (!Character.isUpperCase(first_letter)) {
            return false;
        }

        for (int i = 1; i < name.length(); i++) {
            char c = name.charAt(i);

            if (!Character.isAlphabetic(c)) {
                return false;
            }
        }

        return true;
    }

    //deleting client with existing orders connected to him 
    //requires deleting all that orders and making regarding products available
    public void destroyOrder(int ID) {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //finding all the conections between selected order and products
            PreparedStatement orderStatement = conn.prepareStatement(
                    "select StockID from orderstostocks where OrderID =?");
            orderStatement.setInt(1, ID);
            ResultSet stocks_in_order = orderStatement.executeQuery();

            while (stocks_in_order.next()) {
                int stockID = stocks_in_order.getInt("StockID");
                //making regarding stocks available
                PreparedStatement releaseStock = conn.prepareStatement(
                        "update stocks set Available=1 where StockID=?");
                releaseStock.setInt(1, stockID);
                releaseStock.executeUpdate();
            }
            //deleting connections of stocks to the order
            orderStatement = conn.prepareStatement(
                    "delete from orderstostocks where OrderID =?");
            orderStatement.setInt(1, ID);
            orderStatement.executeUpdate();
            //deleting selected order
            orderStatement = conn.prepareStatement(
                    "delete from orders where OrderID =?");
            orderStatement.setInt(1, ID);
            orderStatement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    private void jButtonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAddActionPerformed
        try {
            String card_number = jTextCardNumber1.getText()
                    + jTextCardNumber2.getText()
                    + jTextCardNumber3.getText()
                    + jTextCardNumber4.getText();
            String name = jTextName.getText();

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            preparedStatement = conn.prepareStatement("insert into clients(Name,Surname,"
                    + "Adress,CardNumber,Type)value(?,?,?,?,?)");
            preparedStatement.setString(1, jTextName.getText());
            preparedStatement.setString(2, jTextSurname.getText());
            preparedStatement.setString(3, jTextAdress.getText());
            preparedStatement.setString(4, card_number);
            preparedStatement.setString(5, jComboBoxType.getSelectedItem().toString());

            //validating passed card number and name
            if (!checkCardNumber(card_number) || !checkName(name)) {
                throw new Exception();
            }

            //addind a new client to the databse
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(this, "Client Added");
            upDate(ALL);
        } catch (SQLException exception) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
            JOptionPane.showMessageDialog(this, "Wrong input data");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Wrong input data");
        }
    }//GEN-LAST:event_jButtonAddActionPerformed

    //closing current window
    private void jButtonQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonQuitActionPerformed
        dispose();
    }//GEN-LAST:event_jButtonQuitActionPerformed

    //oppening orders history window
    private void jButtonOrdersHistoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOrdersHistoryActionPerformed
        new ClientsHistory().setVisible(true);
    }//GEN-LAST:event_jButtonOrdersHistoryActionPerformed

    //deleting selected client
    private void jButtonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonDeleteActionPerformed
        try {
            deleteItem = JOptionPane.showConfirmDialog(null, "Do you want to delete this client?",
                    "Warning", JOptionPane.YES_NO_OPTION);
            if (deleteItem == JOptionPane.YES_OPTION) {
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                //finding all the orders of selected client
                PreparedStatement orderStatement = conn.prepareStatement(
                        "select * from orders where ClientID = ?");
                orderStatement.setString(1, clientID);
                ResultSet orders = orderStatement.executeQuery();
                while (orders.next()) {
                    System.out.println("69 ammirite");
                    int ord_id = orders.getInt("OrderID");
                    destroyOrder(ord_id);
                }

                conn = DriverManager.getConnection(DB_URL, USER, PASS);
                preparedStatement = conn.prepareStatement("delete from clients where ClientID = ?");

                preparedStatement.setString(1, clientID);
                preparedStatement.executeUpdate();
                JOptionPane.showMessageDialog(this, "Client deleted");
                upDate(ALL);

            }
        } catch (SQLException ex) {
            System.err.println(ex);

        }
    }//GEN-LAST:event_jButtonDeleteActionPerformed

    private void jButtonEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEditActionPerformed
        try {
            String card_number = jTextCardNumber1.getText()
                    + jTextCardNumber2.getText()
                    + jTextCardNumber3.getText()
                    + jTextCardNumber4.getText();
            String name = jTextName.getText();

            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            //updating selected client's data with details passed in the text boxes and combo box
            preparedStatement = conn.prepareStatement("update clients set Name=?, Surname=?,"
                    + "Adress=?, CardNumber=?, Type=? where ClientID=?");
            preparedStatement.setString(1, name);
            preparedStatement.setString(2, jTextSurname.getText());
            preparedStatement.setString(3, jTextAdress.getText());
            preparedStatement.setString(4, card_number);
            preparedStatement.setString(5, jComboBoxType.getSelectedItem().toString());
            preparedStatement.setString(6, clientID);

            //validating the card number and the name that have been paseed
            if (!checkCardNumber(card_number) || !checkName(name)) {
                throw new Exception();
            }

            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(this, "Client edited");
            upDate(ALL);
        } catch (SQLException exception) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE,
                    null, exception);
            JOptionPane.showMessageDialog(this, "Wrong input data");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Wrong input data");
        }
    }//GEN-LAST:event_jButtonEditActionPerformed

    // making all the text boxes empty
    private void jButtonResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonResetActionPerformed
        jTextName.setText("");
        jTextSurname.setText("");
        jTextAdress.setText("");
        jTextCardNumber1.setText("");
        jTextCardNumber2.setText("");
        jTextCardNumber3.setText("");
        jTextCardNumber4.setText("");
        jTextAdress.setText("");
    }//GEN-LAST:event_jButtonResetActionPerformed

    //passing selected data from the main table to the text boxes
    private void jTableClientsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableClientsMouseClicked
        DefaultTableModel RecordTable = (DefaultTableModel) jTableClients.getModel();
        int Row = jTableClients.getSelectedRow();
        jComboBoxType.setSelectedItem(RecordTable.getValueAt(Row, 5).toString());
        clientID = RecordTable.getValueAt(Row, 0).toString();
        jTextName.setText(RecordTable.getValueAt(Row, 1).toString());
        jTextSurname.setText(RecordTable.getValueAt(Row, 2).toString());
        jTextAdress.setText(RecordTable.getValueAt(Row, 3).toString());
        String card_number = RecordTable.getValueAt(Row, 4).toString();
        jTextCardNumber1.setText(card_number.substring(0, 4));
        jTextCardNumber2.setText(card_number.substring(4, 8));
        jTextCardNumber3.setText(card_number.substring(8, 12));
        jTextCardNumber4.setText(card_number.substring(12));
            }//GEN-LAST:event_jTableClientsMouseClicked

    private void jSearchParameterComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jSearchParameterComboBoxActionPerformed
        searchParameter = jSearchParameterComboBox.getSelectedItem().toString();
    }//GEN-LAST:event_jSearchParameterComboBoxActionPerformed

    private void jButtonShowAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonShowAllActionPerformed
        upDate(ALL);
    }//GEN-LAST:event_jButtonShowAllActionPerformed


    private void jButtonSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSearchActionPerformed
        //formulating the query
        String parameter = "";
        String sercz = jTextSearchField.getText();
        switch (searchParameter) {
            case "ID":
                parameter = "ClientID";
                break;
            case "Card Number":
                parameter = "CardNumber";
                break;
            default:
                parameter = searchParameter;
        }

        String mySQLcommand = "SELECT * FROM clients WHERE " + parameter + " LIKE '%" + sercz + "%'";

        upDate(mySQLcommand);
    }//GEN-LAST:event_jButtonSearchActionPerformed

    private void jTextCardNumber1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextCardNumber1KeyTyped
        String input = jTextCardNumber1.getText();
        if (input.length() > 3) {
            jTextCardNumber1.setText(input.substring(0, 3));
        }
    }//GEN-LAST:event_jTextCardNumber1KeyTyped

    private void jTextCardNumber2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextCardNumber2KeyTyped
        String input = jTextCardNumber2.getText();
        if (input.length() > 3) {
            jTextCardNumber2.setText(input.substring(0, 3));
        }
    }//GEN-LAST:event_jTextCardNumber2KeyTyped

    private void jTextCardNumber3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextCardNumber3KeyTyped
        String input = jTextCardNumber3.getText();
        if (input.length() > 3) {
            jTextCardNumber3.setText(input.substring(0, 3));
        }
    }//GEN-LAST:event_jTextCardNumber3KeyTyped

    private void jTextCardNumber4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextCardNumber4KeyTyped
        String input = jTextCardNumber4.getText();
        if (input.length() > 3) {
            jTextCardNumber4.setText(input.substring(0, 3));
        }
    }//GEN-LAST:event_jTextCardNumber4KeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientsBrowse.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClientsBrowse().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdd;
    private javax.swing.JButton jButtonDelete;
    private javax.swing.JButton jButtonEdit;
    private javax.swing.JButton jButtonOrdersHistory;
    private javax.swing.JButton jButtonQuit;
    private javax.swing.JButton jButtonReset;
    private javax.swing.JButton jButtonSearch;
    private javax.swing.JButton jButtonShowAll;
    private javax.swing.JComboBox<String> jComboBoxType;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox<String> jSearchParameterComboBox;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTableClients;
    private javax.swing.JTextField jTextAdress;
    private javax.swing.JTextField jTextCardNumber1;
    private javax.swing.JTextField jTextCardNumber2;
    private javax.swing.JTextField jTextCardNumber3;
    private javax.swing.JTextField jTextCardNumber4;
    private javax.swing.JTextField jTextName;
    private javax.swing.JTextField jTextSearchField;
    private javax.swing.JTextField jTextSurname;
    // End of variables declaration//GEN-END:variables
}
