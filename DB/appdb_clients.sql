-- MySQL dump 10.13  Distrib 8.0.25, for Win64 (x86_64)
--
-- Host: localhost    Database: appdb
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `ClientID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Surname` varchar(45) NOT NULL,
  `Adress` varchar(45) NOT NULL,
  `CardNumber` varchar(16) NOT NULL,
  `Type` enum('Individual','Company') NOT NULL,
  PRIMARY KEY (`ClientID`),
  UNIQUE KEY `idClients_UNIQUE` (`ClientID`),
  UNIQUE KEY `CardNumber_UNIQUE` (`CardNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` VALUES (2,'Jessika','Nowak','Szkolna 5','3524367611113335','Individual'),(3,'Paw','Sm','No','2345367624413333','Company'),(4,'Pawel','Smith','Nowowiejska','2345456324413333','Individual'),(7,'John','Lee','Baker Street 21','2454575689893333','Individual'),(8,'Paul','Smith','Pool Street 21','9876234411113333','Individual'),(10,'Oleg','Szmyrlow','Sasina 5','2423575689893337','Individual'),(11,'Yahor','Awow','Abbey 3 NewEast','4356889889893337','Company'),(12,'Wlad','Putnim','Budziszyn 5/2','6677889889893337','Company'),(13,'Paw','Sm','N','2345883889893337','Company'),(14,'Jessika','Nowak','Szkolna 5','3524367622222222','Individual'),(15,'Jarek','Ceneusz','Plac Wilsona 69','1234123412341232','Individual'),(16,'Jarek','Ceneusz','Plac Wilsona 69','1234123412341234','Individual'),(21,'Jessika','Nowak','Szkolna ','3524367622522222','Individual');
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-18 17:32:39
